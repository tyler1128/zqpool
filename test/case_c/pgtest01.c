#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpq-fe.h>
#include <arpa/inet.h>


uint64_t htonll(uint64_t host)
{
        unsigned long temp_low, temp_high;
        temp_low = htonl((long)host);
        temp_high = htonl((long)(host >> 32));
 
        host &= 0;
        host |= temp_low;
        host <<= 32; 
        host |= temp_high;
        return host;
}


int main(int argc, char * argv[])
{
    PGconn *conn;
    PGresult * res;
    ConnStatusType pgstatus;
    //char connstr[1024];
    char * connstr;
    char szSQL[2048];
    int nParams = 0;
    int paramLengths[5];
    int paramFormats[5];
    Oid paramTypes[5];
    char * paramValues[5];

    int i, cnt;
    //char parm1[32];
    //åchar parm2[32];
    int k;

    if (argc != 2) 
    {
        printf("Example: %s 'hostaddr=127.0.0.1 dbname=sundb port=5436 user=kuafu password=chase'\n", argv[0]);
        return -1;
    }

    connstr = argv[1];

    // sprintf(connstr,
    //         "hostaddr=%s dbname=%s port=%d user=%s password=%s",
    //         "127.0.0.1", "mydb", port, "scott", "tiger");

    conn = PQconnectdb(connstr);
    pgstatus = PQstatus(conn);
    if (pgstatus == CONNECTION_OK)
    {
        printf("Connect database success!\n");
    }
    else
    {
        printf("Connect database failed: %s\n",PQerrorMessage(conn));
        return -1;
    }
    
    sprintf(szSQL, "SELECT id,i1,i2, c1, c2, t1 FROM testtab01 WHERE i1 = $1 and i2 = $2");

    paramTypes[0] = 23;
    paramTypes[1] = 20;

    res = PQprepare(conn,
                    "tangS1",
                    szSQL,
                    2,
                    //NULL);
                    paramTypes);


   if( PQresultStatus(res) != PGRES_COMMAND_OK )
    {
        printf("Failed to prepare SQL : %s\n: %s\n",szSQL, PQerrorMessage(conn));
        PQfinish(conn);
        return -1;
    }
    PQclear(res);
    
    int param1;
    int64_t param2;
    paramValues[0] = (char *)&param1;
    paramValues[1] = (char *)&param2;

    
    for (k=0; k<2; k++)
    {
        //sprintf(cid, "%d", 50260+k);
        //sprintf(parm1, "%d", 10);
        //sprintf(parm2, "%d", 100);
        param1 = 10;
        param1 = htonl(param1);
        param2 = 10;
        param2 = htonll(param2);
        
        paramLengths[0] = 4;
        paramFormats[0] = 1;

        paramLengths[1] = 8;
        paramFormats[1] = 1;

        res = PQexecPrepared(conn,
                             "tangS1",
                             2,
                             paramValues,
                             paramLengths,
                             paramFormats,
                             1
            );
        if(  (PQresultStatus(res) != PGRES_COMMAND_OK ) && (PQresultStatus(res) != PGRES_TUPLES_OK))
        {
            printf("%s\n",PQerrorMessage(conn));
            PQclear(res);
            PQfinish(conn);
            return -1;
        }

        cnt = PQntuples(res);
        printf("return %d rows\n", cnt);
        for (i=0; i<cnt; i++)
        {
            printf("row %d: %d\n", i, ntohl(*(int*)PQgetvalue(res, i, 0)));
        }

        PQclear(res);
    }



    /* 关闭数据库连接并清理 */
    PQfinish(conn);
    return 0;
}
