import java.sql.*;
import java.io.*;
import java.util.*;

class benchselect extends Thread
{
    int runcnt;
    String runsql;
    int threadid;
    String dburl;
    String username;
    String password;
    
    public benchselect(String arg_url,String arg_user,String arg_pass,int id,int x, String n)
    {
        dburl = arg_url;
        username = arg_user;
        password = arg_pass;
        runcnt = x;
        runsql = n;
        threadid=id;
    }

    public void run()
    {
 
        System.out.println("Thread "+threadid+" start...");
        
        Connection conn=null;
        ResultSet rsget=null;
        PreparedStatement pstmtget=null;
        PreparedStatement pstmtmain=null;
        int  iMaxid=0;
        
        try
        {
            //第一步，建立驱动程序实例
            //Class.forName("oracle.jdbc.driver.OracleDriver"); 
            Class.forName("org.postgresql.Driver");
            
            //第二步，获得连接 
            conn = DriverManager.getConnection(dburl, username,password);             
            conn.setAutoCommit(false);            
            // 
           

            pstmtget = conn.prepareStatement("select max(id) maxid from table_id");
            rsget = pstmtget.executeQuery();
            if (rsget.next())
            {
                iMaxid=rsget.getInt(1);
            }
            else
            {
                iMaxid=10000;
            }
            rsget.close();
            rsget=null;
            pstmtget.close();
            pstmtget=null;      
            
            pstmtget = conn.prepareStatement("select pk from table_id where id=?");
            
            pstmtmain = conn.prepareStatement(runsql);
  
            
            //第四步，执行SQL语句
            
            Random rand = new Random();
            java.util.Date rtS=new java.util.Date();
                        
            for(int i=0;i<runcnt;i++)
            { 
                pstmtget.setInt(1,rand.nextInt(iMaxid));
                rsget = pstmtget.executeQuery();
                String strGet;
                if (rsget.next())
                {
                    strGet=rsget.getString(1);
                }
                else
                {
                	  strGet=null;
                }
                rsget.close();
                rsget=null;
                pstmtmain.setString(1,strGet);                
                //ResultSet rs = pstmtmain.executeQuery();
                pstmtmain.executeQuery();
                //处理结果集
                /*
                while(rs.next()) 
                { 
                   String ename = rs.getString(1); 
                   //System.out.println("ename: " + ename); 
                }
                */  
                //rs.close();
            }
            java.util.Date rtE=new java.util.Date();
            System.out.println("thread "+threadid+" run time:"+(rtE.getTime()-rtS.getTime())+"ms.");             
            
            pstmtmain.close();
            pstmtmain=null;
            pstmtget.close();
            pstmtget=null;
            conn.close();
            conn=null;                      	  
        }
        catch (java.sql.SQLException e)
        {
        	System.out.println(e);
        }
        catch(Exception e)
        {       
            System.out.println(e);
        }                   
    }

    static public void main(String args[])
    {
        //"jdbc:oracle:thin:@localhost:1521:boss"
        if (args.length!=6)
        {
            System.out.printf("args.length=%d\n", args.length);
        	System.out.println("Example: ./benchselect.sh <url> <username> <password> <threadcount> <runcount> <sql>\n");
        	System.out.println("Example: ./benchselect.sh jdbc:postgresql://127.0.0.1:5436/sundb kuafu chase 2 100 'select t from bench_select where pk=?'\n");
        	return;
        }

        String runsql;
        String dburl = args[0];
        String username = args[1];
        String password = args[2];
        System.out.println("password="+password);
        
        int threadcnt=Integer.parseInt(args[3]); 
        System.out.println("threadcnt:"+threadcnt);
        int runcnt=Integer.parseInt(args[4]);
        System.out.println("runcnt="+runcnt);
        runsql=args[5];        
        
        int i;
        benchselect[] tp;
        tp=new benchselect[threadcnt];
    
        for(i=0;i<threadcnt;i++)
        {
            tp[i]=new benchselect(dburl, username,password,i,runcnt,runsql);        	
        } 
        for(i=0;i<threadcnt;i++)
        {
            tp[i].start();
        }
        
    }
}

