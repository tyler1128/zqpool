# zqpool

#### Description

ZQPool is an open source connection pool software for PostgreSQL released by CSUDATA.COM. It mainly solves the following two shortcomings of the popular connection pool software pgbouncer:

1. Solve the problem that the JDBC program cannot reduce the number of connections on the database by using pgbouncer;
2. Pgbouncer cannot use the multi-core of CPU. When the processing capacity exceeds the capacity of single core in the case of high concurrency, the performance is pool.


#### Software Architecture

The application connects to zqpool, which connects to the PostgreSQL database.

#### Installation

The installation step is as follows:
1. Copy zqpool1 0.x86_64.xz to a directory
2. Decompression: tar -xf zqpool1.0.x86_64.xz
3. After decompression, there are two files zqpool.conf and zqpool, where zqpool.conf is a configuration file, which is configured accordingly.
4. Then start zqpool.


#### Instructions


Configure zqpool Conf file, each configuration item is described as follows:

* listen_port = 5436     ： set the listening port of zqpool
* listen_addr = *        : set the listening IP of zqpool, when set to "*", which means listening on all local IP addresses
* default_pool_size = 10 :   sets the total number of connections to the backend database
* max_client_conn = 3000 ： the total number of applications allowed to connect to zqpool

* db.1.user=u01            ： user name of the first database
* db.1.dbname=postgres     ： the first database name
* db.1.ipport=172.22.224.10:5432  ： ip address and port of the first database
* db.1.passwd=u01                 :  the password of the first database

Configure file is zqpool.conf, each configuration item is described as follows

* listen_port = 5436     ：set the listening port of zqpool
* listen_addr = *        : set the listening IP of zqpool, when set to "*", which means listening on all local IP addresses
* mgr_port = 9380        : set management port of zqpool
* mgr_addr = *           : 

every pool setup is：

* pool.1.fe_max_conns = 3000
* pool.1.fe_user=u01
* pool.1.fe_passwd=u02
* pool.1.fe_dbname=mydb
* pool.1.be_user=u01
* pool.1.be_passwd=u01
* pool.1.be_dbname=postgres
* pool.1.be_conns = 10
* pool.1.be_ipport=172.22.224.10:5432,172.22.224.10:5411
* pool.1.be_conn_life_time=60  # specify the life_time of backend connection

pool.1 represents the first pool, and there can also be pool.2, pool.3 and etc.  pool.1.be_ipport can be a list of  multiple ip address ports that  separated by commas.


start zqpool:

```
[codetest@pgdev zqpool]$ ./zqpool
2022/05/24 09:12:30 server.go:2188: Starting server on :5436 ...
```

in another terminal windows，use psql connect to zqpool：

```
[codetest@pgdev zqpool]$ /usr/pgsql-10/bin/psql -h 172.22.224.10 -p 5436 -Uu01 -d postgres
Password for user u01:
psql (10.20, server 10.5)
Type "help" for help.


postgres=> \d
         List of relations
 Schema |  Name  | Type  |  Owner
--------+--------+-------+----------
 public | test01 | table | postgres
(1 row)

postgres=> select * from test01;
 id | t
----+---
(0 rows)

postgres=> insert into test01 values(1, '111'),(2,'222'),(3,'333');
INSERT 0 3
postgres=> select * from test01;
 id |  t
----+-----
  1 | 111
  2 | 222
  3 | 333
(3 rows)
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
